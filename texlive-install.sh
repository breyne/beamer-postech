#!/bin/sh

######################################################################
RED='\033[0;33m'
NC='\033[0m' # No Color

######################################################################
package="보스택"
dir=$(pwd)

echo -e ${RED}
echo [${package}] LINK FILES TO LOCAL TEXMF
echo [${package}] Initialize usertree
echo -e ${NC}
tlmgr init-usertree

#cd ~/texmf
cd $(kpsewhich -var-value TEXMFHOME) 
mkdir -p tex/latex/local/beamer/ && cd tex/latex/local/beamer/

######################################################################
echo -e ${RED}
echo [${package}] source directory: $dir
echo [${package}] destination dir : $(pwd)
echo -e ${NC}

for file in $dir/*.sty 
do
 echo $file
 ln -s $file .
done

for file in $dir/postech_*.pdf
do
 echo $file
 ln -s $file .
done

echo -e ${RED}
echo [${package}] rebuild index
echo -e ${NC}
texhash ~/texmf

echo -e ${RED}
echo [${package}] Over. 
echo -e ${NC}
