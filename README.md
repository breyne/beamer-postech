# Postech beamer theme

A minimalistic beamer (color) theme for POSTECH: *Pohang University of Science and Technology*.

The graphics and colors definitions come from the official website:
www.postech.ac.kr/eng/about-postech/introduction-to-postech/symbols/

More details in the example `postech-example.{tex,pdf}`.